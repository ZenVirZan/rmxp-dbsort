class RGSS_Remapper
  def remap_system
    filename = 'Data/System.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data.switches = remap_array(data.switches, @switch_map)
    data.variables = remap_array(data.variables, @variable_map)
    data.elements = remap_array(data.elements, @element_map)
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end
  
  def remap_actors
    filename = 'Data/Actors.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @actor_map)
    for actor in data
      next if actor == nil
      actor.class_id = @class_map[actor.class_id]
      actor.weapon_id = @weapon_map[actor.weapon_id]
      actor.armor1_id = @armor_map[actor.armor1_id]
      actor.armor2_id = @armor_map[actor.armor2_id]
      actor.armor3_id = @armor_map[actor.armor3_id]
      actor.armor4_id = @armor_map[actor.armor4_id]
    end
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end
  
  def remap_animations
    filename = 'Data/Animations.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @animation_map)
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end
  
  def remap_armors
    filename = 'Data/Armors.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @armor_map)
    for armor in data
      next if armor == nil
      armor.auto_state_id = @state_map[armor.auto_state_id]
      armor.guard_state_set = remap_array(armor.guard_state_set, @state_map)
      armor.guard_element_set = armor.guard_element_set.map {|s| @element_map[s]}
    end
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end

  def remap_classes
    filename = 'Data/Classes.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @class_map)
    for klass in data
      next if klass == nil
      klass.weapon_set = klass.weapon_set.map {|v| @weapon_map[v]}
      klass.armor_set = klass.armor_set.map {|v| @armor_map[v]}
      klass.state_ranks = remap_1d_table(klass.state_ranks, @state_map)
      klass.element_ranks = remap_1d_table(klass.element_ranks, @element_map)
      for learning in klass.learnings
        learning.skill_id = @skill_map[learning.skill_id]
      end
    end
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end

  def remap_commonevents
    filename = 'Data/CommonEvents.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @common_event_map)
    for ev in data
      next if ev == nil
      ev.switch_id = @switch_map[ev.switch_id]
      remap_eventlist(ev.list)
    end
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end

  def remap_enemies
    filename = 'Data/Enemies.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @enemy_map)
    for enemy in data
      next if enemy == nil
      enemy.animation1_id = @animation_map[enemy.animation1_id]
      enemy.animation2_id = @animation_map[enemy.animation2_id]
      enemy.state_ranks = remap_1d_table(enemy.state_ranks, @state_map)
      enemy.element_ranks = remap_1d_table(enemy.element_ranks, @element_map)
      for action in enemy.actions
        action.skill_id = @skill_map[action.skill_id]
        action.condition_switch_id = @switch_map[action.condition_switch_id]
      end
      enemy.item_id = @item_map[enemy.item_id]
      enemy.weapon_id = @weapon_map[enemy.weapon_id]
      enemy.armor_id = @armor_map[enemy.armor_id]
    end
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end

  def remap_items
    filename = 'Data/Items.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @item_map)
    for item in data
      next if item == nil
      item.animation1_id = @animation_map[item.animation1_id]
      item.animation2_id = @animation_map[item.animation2_id]
      item.common_event_id = @common_event_map[item.common_event_id]
      item.plus_state_set = item.plus_state_set.map {|s| @state_map[s]}
      item.minus_state_set = item.minus_state_set.map {|s| @state_map[s]}
      item.element_set = item.element_set.map {|s| @element_map[s]}
    end
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end

  def remap_skills
    filename = 'Data/Skills.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @skill_map)
    for skill in data
      next if skill == nil
      skill.animation1_id = @animation_map[skill.animation1_id]
      skill.animation2_id = @animation_map[skill.animation2_id]
      skill.common_event_id = @common_event_map[skill.common_event_id]
      skill.plus_state_set = skill.plus_state_set.map {|s| @state_map[s]}
      skill.minus_state_set = skill.minus_state_set.map {|s| @state_map[s]}
      skill.element_set = skill.element_set.map {|s| @element_map[s]}
    end
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end

  def remap_states
    filename = 'Data/States.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @state_map)
    for state in data
      next if state == nil
      state.animation_id = @animation_map[state.animation_id]
      state.plus_state_set = state.plus_state_set.map {|s| @state_map[s]}
      state.minus_state_set = state.minus_state_set.map {|s| @state_map[s]}
      state.guard_element_set = state.guard_element_set.map {|s| @element_map[s]}
    end
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end

  def remap_tilesets
    filename = 'Data/Tilesets.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @tileset_map)
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end
  
  def remap_troops
    filename = 'Data/Troops.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @troop_map)
    for troop in data
      next if troop == nil
      troop.members.each {|m| m.enemy_id = @enemy_map[m.enemy_id]}
      for page in troop.pages
        remap_eventlist(page.list)
        page.condition.switch_id = @switch_map[page.condition.switch_id]
        page.condition.actor_id = @actor_map[page.condition.actor_id]
      end
    end
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end

  def remap_weapons
    filename = 'Data/Weapons.rxdata'
    input_file = File.join(@project_path, filename)
    data = load(input_file)
    data = remap_array(data, @weapon_map)
    for weapon in data
      next if weapon == nil
      weapon.animation1_id = @animation_map[weapon.animation1_id]
      weapon.animation2_id = @animation_map[weapon.animation2_id]
      weapon.plus_state_set = weapon.plus_state_set.map {|s| @state_map[s]}
      weapon.minus_state_set = weapon.minus_state_set.map {|s| @state_map[s]}
      weapon.element_set = weapon.element_set.map {|s| @element_map[s]}
    end
    output_file = File.join(@output_path, filename)
    return save(data, output_file)
  end

  def remap_map(map_file)
    input_file = File.join(@project_path, 'Data', map_file)
    data = load(input_file)
    data.tileset_id = @tileset_map[data.tileset_id]
    data.encounter_list = data.encounter_list.map {|e| @troop_map[e]}
    for id in data.events.keys
      ev = data.events[id]
      for page in ev.pages
        page.condition.switch1_id = @switch_map[page.condition.switch1_id]
        page.condition.switch2_id = @switch_map[page.condition.switch2_id]
        page.condition.variable_id = @variable_map[page.condition.variable_id]
        remap_eventlist(page.list)
      end
    end
    output_file = File.join(@output_path, 'Data', map_file)
    return save(data, output_file)
  end

  def remap_eventlist(list)
    index = 0
    while index < list.size
      command = list[index]
      params = command.parameters
      case command.code
      when 101  # Show Text
        params[0] = remap_text(params[0])
      when 401  # Show Text
        params[0] = remap_text(params[0])
      when 102  # Show Choices
        params[0] = params[0].map {|s| remap_text(s)}
      when 402  # When ...
        params[1] = remap_text(params[1])
      when 103  # Input Number
        params[0] = @variable_map[params[0]]
      when 105  # Button Input Processing
        params[0] = @variable_map[params[0]]
      when 111  # Conditional Branch
        case params[0]
        when 0
          params[1] = @switch_map[params[1]]
        when 1
          params[1] = @variable_map[params[1]]
          if params[2] != 0
            params[3] = @variable_map[params[3]]
          end
        when 4
          params[1] = @actor_map[params[1]]
          case params[2]
          when 2
            params[3] = @skill_map[params[3]]
          when 3
            params[3] = @weapon_map[params[3]]
          when 4
            params[3] = @armor_map[params[3]]
          when 5
            params[3] = @state_map[params[3]]
          end
        when 5
          if params[2] == 1
            params[3] = @state_map[params[3]]
          end
        when 8
          params[1] = @item_map[params[1]]
        when 9
          params[1] = @weapon_map[params[1]]
        when 10
          params[1] = @armor_map[params[1]]
        end
      when 117  # Call Common Event
        params[0] = @common_event_map[params[0]]
      when 121  # Control Switches
        mapped_switches = (params[0]..params[1]).map{|s| @switch_map[s]}
        switch_groups = mapped_switches.sort.chunk_while {|a,b| a+1==b}.to_a
        if @force_split_bulk_commands and switch_groups.size > 1
          params[0] = mapped_switches[0]
          params[1] = mapped_switches[0]
          for i in 1...mapped_switches.size
            new_params = params.clone
            new_params[0] = mapped_switches[i]
            new_params[1] = mapped_switches[i]
            list.insert(index+1, RPG::EventCommand.new(command.code,command.indent,new_params))
            index += 1
          end
        else
          params[0] = switch_groups[0].min
          params[1] = switch_groups[0].max
          for group_id in 1...switch_groups.size
            new_params = params.clone
            new_params[0] = switch_groups[group_id].min
            new_params[1] = switch_groups[group_id].max
            list.insert(index+1, RPG::EventCommand.new(command.code,command.indent,new_params))
            index += 1
          end
        end
      when 122  # Control Variables
        case params[3]
        when 1
          params[4] = @variable_map[params[4]]
        when 3
          params[4] = @item_map[params[4]]
        when 4
          params[4] = @actor_map[params[4]]
        end
        mapped_variables = (params[0]..params[1]).map{|s| @variable_map[s]}
        variable_groups = mapped_variables.sort.chunk_while {|a,b| a+1==b}.to_a
        if @force_split_bulk_commands and variable_groups.size > 1
          params[0] = mapped_variables[0]
          params[1] = mapped_variables[0]
          for i in 1...mapped_variables.size
            new_params = params.clone
            new_params[0] = mapped_variables[i]
            new_params[1] = mapped_variables[i]
            list.insert(index+1, RPG::EventCommand.new(command.code,command.indent,new_params))
            index += 1
          end
        else
          params[0] = variable_groups[0].min
          params[1] = variable_groups[0].max
          for group_id in 1...variable_groups.size
            new_params = params.clone
            new_params[0] = variable_groups[group_id].min
            new_params[1] = variable_groups[group_id].max
            list.insert(index+1, RPG::EventCommand.new(command.code,command.indent,new_params))
            index += 1
          end
        end
      when 125  # Change Gold
        if params[1] == 1
          params[2] = @variable_map[params[2]]
        end
      when 126  # Change Items
        params[0] = @item_map[params[0]]
        if params[2] == 1
          params[3] = @variable_map[params[3]]
        end
      when 127  # Change Weapons
        params[0] = @weapon_map[params[0]]
        if params[2] == 1
          params[3] = @variable_map[params[3]]
        end
      when 128  # Change Armor
        params[0] = @armor_map[params[0]]
        if params[2] == 1
          params[3] = @variable_map[params[3]]
        end
      when 129  # Change Party Member
        params[0] = @actor_map[params[0]]
      when 201  # Transfer Player
        if params[0] != 0
          params[1] = @variable_map[params[1]]
          params[2] = @variable_map[params[2]]
          params[3] = @variable_map[params[3]]
        end
      when 202  # Set Event Location
        if params[1] == 1
          params[2] = @variable_map[params[2]]
          params[3] = @variable_map[params[3]]
        end
      when 207  # Show Animation
        params[1] = @animation_map[params[1]]
      when 209  # Set Move Route
        remap_move_route(params[1].list)
      when 231  # Show Picture
        if params[3] != 0
          params[4] = @variable_map[params[4]]
          params[5] = @variable_map[params[5]]
        end
      when 232  # Move Picture
        if params[3] != 0
          params[4] = @variable_map[params[4]]
          params[5] = @variable_map[params[5]]
        end
      when 301  # Battle Processing
        params[0] = @troop_map[params[0]]
      when 302,605  # Shop Processing
        case params[0]
        when 0
          params[1] = @item_map[params[1]]
        when 1
          params[1] = @weapon_map[params[1]]
        when 2
          params[1] = @armor_map[params[1]]
        end
      when 303  # Name Input Processing
        params[0] = @actor_map[params[0]]
      when 311  # Change HP
        if params[2] == 1
          params[3] = @variable_map[params[3]]
        end
        if params[0] > 0
          params[0] = @actor_map[params[0]]
        end
      when 312  # Change SP
        if params[2] == 1
          params[3] = @variable_map[params[3]]
        end
        if params[0] > 0
          params[0] = @actor_map[params[0]]
        end
      when 313  # Change State
        params[2] = @state_map[params[2]]
        if params[0] > 0
          params[0] = @actor_map[params[0]]
        end
      when 314  # Recover All
        if params[0] > 0
          params[0] = @actor_map[params[0]]
        end
      when 315  # Change EXP
        if params[2] == 1
          params[3] = @variable_map[params[3]]
        end
        if params[0] > 0
          params[0] = @actor_map[params[0]]
        end
      when 316  # Change Level
        if params[2] == 1
          params[3] = @variable_map[params[3]]
        end
        if params[0] > 0
          params[0] = @actor_map[params[0]]
        end
      when 317  # Change Parameters
        if params[3] == 1
          params[4] = @variable_map[params[4]]
        end
        params[0] = @actor_map[params[0]]
      when 318  # Change Skills
        params[0] = @actor_map[params[0]]
        params[2] = @skill_map[params[2]]
      when 319  # Change Equipment
        params[0] = @actor_map[params[0]]
        case params[1]
        when 0
          params[2] = @weapon_map[params[2]]
        when 1,2,3,4
          params[2] = @armor_map[params[2]]
        end
      when 320  # Change Actor Name
        params[0] = @actor_map[params[0]]
      when 321  # Change Actor Class
        params[0] = @actor_map[params[0]]
        params[1] = @class_map[params[1]]
      when 322  # Change Actor Graphic
        params[0] = @actor_map[params[0]]
      when 331  # Change Enemy HP
        if params[2] == 1
          params[3] = @variable_map[params[3]]
        end
      when 332  # Change Enemy SP
        if params[2] == 1
          params[3] = @variable_map[params[3]]
        end
      when 333  # Change Enemy State
        params[2] = @state_map[params[2]]
      when 337  # Show Battle Animation
        params[2] = @animation_map[params[2]]
      when 338  # Deal Damage
        if params[2] != 0
          params[3] = @variable_map[params[3]]
        end
      when 339  # Force Action
        if params[2] != 0
          params[3] = @skill_map[params[3]]
        end
      when 355  # Script

      end
      index += 1
    end
  end

  def remap_move_route(list)
    for command in list
      params = command.parameters
      case command.code
      when 27,28
        params[0] = @switch_map[params[0]]
      end
    end
  end

  def remap_text(text)
    new_text = text.gsub(/\\([Vv])\[([0-9]+)\]/) { "\\#{$1}[#{@variable_map[$2.to_i]}]" }
    new_text.gsub!(/\\([Nn])\[([0-9]+)\]/) { "\\#{$1}[#{@actor_map[$2.to_i]}]" }
    if (@verbose and text != new_text)
      puts "Remapped text: \"#{text}\" -> \"#{new_text}\""
    end
    return new_text
  end
end
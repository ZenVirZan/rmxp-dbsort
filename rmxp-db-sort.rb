require_relative 'rgss.rb'
require_relative 'remap.rb'
require_relative 'command_param.rb'

class Container 
  attr_accessor :index, :value
  def initialize(index, value)
    @index = index
    @value = value
  end
end

class RGSS_Remapper
  def run(project_path, output_path = nil, allow_overwrite_source = false, allow_overwrite = false, 
    skip_confirmations = false, verbose = false, force_split_bulk_commands = false)
    @verbose = verbose
    @force_split_bulk_commands = force_split_bulk_commands

    @project_path = File.expand_path(project_path)
    if File.extname(@project_path) == 'rxproj'
      @project_path = File.dirname(@project_path)
    end

    @output_path = output_path ? File.expand_path(output_path) : @project_path
    if not File.directory?(@output_path)
      puts "Output path must be a directory"
      return 1
    end

    data_dir = File.join(@output_path,"Data")
    if not Dir.exist?(data_dir)
      begin
        Dir.mkdir(data_dir)
      rescue => ex
        puts "Unable to create directory 'Data' in #{@output_path}: #{ex.message}"
        return 1
      end
    end

    @allow_overwrite_source = allow_overwrite_source
    @allow_overwrite = allow_overwrite or allow_overwrite_source
    if @project_path.downcase == @output_path.downcase
      if not @allow_overwrite_source
        puts "WARNING: YOU ARE ATTEMPTING TO OVERWRITE YOUR SOURCE PROJECT DATA"
        puts "PLEASE MAKE SURE YOU HAVE BACKED UP YOUR PROJECT BEFORE CONTINUING"
        puts "Use the --overwrite-source flag to overwrite the source project data (DANGEROUS)."
        puts "Use the --output-path argument to save the sorted data to another location"
        return 1
      elsif not skip_confirmations
        puts "WARNING: You are about to overwrite your source project data!"
        puts "BACK UP YOUR PROJECT FIRST!"
        puts "Do you wish to continue? (y/N)"
        yes_no = ''
        while (not ['y','n'].include? yes_no)
          yes_no = gets.downcase
        end
        if yes_no == 'n'
          return 0
        end
      end
    end

    puts "Using project path: " + @project_path
    puts
    puts "Sorting items..."
    @animation_map = map_file(File.join(@project_path, 'Data/Animations.rxdata'))
    @actor_map = map_file(File.join(@project_path, 'Data/Actors.rxdata'))
    @class_map = map_file(File.join(@project_path, 'Data/Classes.rxdata'))
    @item_map = map_file(File.join(@project_path, 'Data/Items.rxdata'))
    @weapon_map = map_file(File.join(@project_path, 'Data/Weapons.rxdata'))
    @armor_map = map_file(File.join(@project_path, 'Data/Armors.rxdata'))
    @common_event_map = map_file(File.join(@project_path, 'Data/CommonEvents.rxdata'))
    @enemy_map = map_file(File.join(@project_path, 'Data/Enemies.rxdata'))
    @troop_map = map_file(File.join(@project_path, 'Data/Troops.rxdata'))
    @state_map = map_file(File.join(@project_path, 'Data/States.rxdata'))
    @skill_map = map_file(File.join(@project_path, 'Data/Skills.rxdata'))
    @tileset_map = map_file(File.join(@project_path, 'Data/Tilesets.rxdata'))
    
    @variable_map = {}
    @switch_map = {}
    @element_map = {}
    map_system
    
    puts
    puts "Writing data..."
    failure = false
    begin 
      failure ||= (not remap_system)
      failure ||= (not remap_actors)
      failure ||= (not remap_animations)
      failure ||= (not remap_armors)
      failure ||= (not remap_classes)
      failure ||= (not remap_commonevents)
      failure ||= (not remap_enemies)
      failure ||= (not remap_items)
      failure ||= (not remap_skills)
      failure ||= (not remap_states)
      failure ||= (not remap_tilesets)
      failure ||= (not remap_troops)
      failure ||= (not remap_weapons)
      failure ||= (not remap_maps)
    rescue => ex
      puts
      puts "An error occurred while writing project data: " + ex.message 
      puts @verbose ? ex.backtrace.join("\n") : "" if @verbose
      failure = true
    end

    puts
    if failure
      puts "Failed to reorder project."
      return 1
    else
      puts "Successfully reordered project!"
      puts "Organised data can be found in: #{File.join(@output_path,'Data')}"
    end
    return 0
  end
  
  def load(path)
    result = nil
    puts "Reading file #{@verbose ? path : File.basename(path)}..."
    File.open(path,'rb') do |f| 
      result = Marshal.load(f)
    end
    return result
  end
  
  def save(object, path)
    puts "Writing file #{@verbose ? path : File.basename(path)}..."
    if File.exist?(path) and not @allow_overwrite
      puts "File already exists: #{path}"
      puts "Use the --overwrite flag to overwrite existing files."
      return false
    end
    File.open(path,'wb') do |f| 
      Marshal.dump(object, f)
    end
    return true
  end
    
  def array_to_container(array)
    container_array = []
    for i in 0...array.size
      container_array[i] = Container.new(i, array[i])
    end
    return container_array
  end
  
  def map_file(path)
    data = load(path)
    data_container = array_to_container(data)
    data_container.sort_by! { |c| [c.value.nil? ? 0 : c.value.name.empty? ? 2 : 1, c.value ? c.value.name : '']}
    map = data_container.map.with_index {|v,i| [v.index,i]}
    return map.to_h
  end
  
  def remap_array(array, map)
    result = [nil]*array.size
    for i in 0...array.size
      result[map[i]] = array[i]
    end
    return result
  end

  def remap_1d_table(table, map)
    result = Table.new(table.xsize)
    for i in 0...table.xsize
      result[map[i]] = table[i]
    end
    return result
  end
  
  def map_system
    system = load(File.join(@project_path, 'Data/System.rxdata'))
    system.switches[0] = nil
    system.variables[0] = nil
    system.elements[0] = nil

    switches = array_to_container(system.switches)
    switches.sort_by! { |c| [c.value == nil ? 0 : c.value.empty? ? 2 : 1, c.value]}
    @switch_map = switches.map.with_index {|v,i| [v.index,i]}.to_h
    
    variables = array_to_container(system.variables)
    variables.sort_by! { |c| [c.value == nil ? 0 : c.value.empty? ? 2 : 1, c.value]}
    @variable_map = variables.map.with_index {|v,i| [v.index,i]}.to_h

    elements = array_to_container(system.elements)
    elements.sort_by! { |c| [c.value == nil ? 0 : c.value.empty? ? 2 : 1, c.value]}
    @element_map = elements.map.with_index {|v,i| [v.index,i]}.to_h
  end
  
  def remap_maps
    map_files = []
    for file in Dir.entries(File.join(@project_path, 'Data'))
      next if !/Map\d\d\d.rxdata/.match?(file)
      map_files.push(file)
    end
    for file in map_files
      remap_map(file)
    end
  end
end

template_params = []
template_params.push Command_Param.new("project-path", "p", :indexed, :string, "Source RMXP project directory path, or path to .rxproj file")
template_params.push Command_Param.new("output-path", "o", :named, :string, "Sorted data file output directory")
template_params.push Command_Param.new("overwrite", "f", :flag, :bool, "Allow overwriting existing files")
template_params.push Command_Param.new("overwrite-source", "s", :flag, :bool, "Allow source project files to be overwritten")
template_params.push Command_Param.new("skip-confirmation", "y", :flag, :bool, "Do not prompt for confirmation when overwriting source data")
template_params.push Command_Param.new("split-bulk-commands", "c", :flag, :bool, "Force bulk switch/variable commands to be split individually if a group needs to be broken apart instead of into smaller groups where possible")
template_params.push Command_Param.new("verbose", "v", :flag, :bool, "Output additional debugging information")
template_params.push Command_Param.new("help", "h", :flag, :bool, "Show this help screen")

params = Command_Param.parse(ARGV,template_params)
if params == nil
  exit(1)
end

if params["help"]
  puts "RMXP DB Sort by ZenVirZan"
  puts "This tool reorganises RPG Maker XP projects by sorting the database,\nswitch and variable entries by their display name."
  puts "Back up your project before use. Use this tool at your own risk."
  puts
  Command_Param.show_help(template_params)
  exit(0)
end

result = RGSS_Remapper.new.run(params["project-path"], 
  params["output-path"], 
  params["overwrite-source"],
  params["overwrite"],
  params["skip-confirmation"],
  params["verbose"],
  params["split-bulk-commands"])
exit(result)
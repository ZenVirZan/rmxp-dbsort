class Command_Param
  attr_reader :name, :short_name, :param_type, :data_type, :description

  PARAM_TYPE = [
    :indexed,
    :named,
    :flag
  ]

  DATA_TYPE = [
    :int,
    :bool,
    :string
  ]

  def initialize(name, short_name, param_type, data_type, description)
    @name = name
    @short_name = short_name
    @param_type = param_type
    @data_type = data_type
    @description = description
  end
  
  def get_value(string_value)
    value = string_value
    if data_type == :int
      value = string_value.to_i
    elsif data_type == :bool
      value = string_value.to_s == "true"
    else
      value = value.to_s
    end
    return value
  end
  
  def self.parse(argv, params)   
    result = params.map {|p| [p.name, p.data_type == :bool ? false : nil] }.to_h
    argv = argv.map {|a| a.strip }

    consumed_params = []
    i = 0
    while i < argv.size
      if argv[i].start_with?("-")
        if argv[i].start_with?("--")
          param_name = argv[i].gsub(/^(--)/,'')
          matched_param_index = params.index {|p| p.name.casecmp(param_name) == 0 and not consumed_params.include?(p.name)}
        elsif argv[i].start_with?("-")
          param_name = argv[i].gsub(/^(-)/,'')
          matched_param_index = params.index {|p| p.short_name.casecmp(param_name) == 0 and not consumed_params.include?(p.name)}
        end
        if matched_param_index == nil
          puts "Unexpected argument: " + argv[i]
          return nil
        end
  
        param = params[matched_param_index]
        case param.param_type
        when :named
          if i+1 < argv.size
            result[param.name] = param.get_value(argv[i+1])
            consumed_params.push(param.name)
            i += 1
          else
            puts "Missing value for input parameter: " + argv[i]
            return nil
          end
        when :flag
          result[param.name] = param.get_value(true)
          consumed_params.push(param.name)
        end
      else
        next_indexed_param = params.filter {|p| p.param_type == :indexed and not consumed_params.include?(p.name) }.first
        if next_indexed_param == nil
          puts "Unexpected argument: " + argv[i]
          return nil
        end
        result[next_indexed_param.name] = next_indexed_param.get_value(argv[i])
        consumed_params.push(next_indexed_param.name)
      end
      i += 1
    end
    return result
  end

  def self.show_help(params) 
    indexed_params = params.filter {|p| p.param_type == :indexed }
    indexed_param_string = indexed_params.map {|p| "<#{p.name}>"}.join(" ")
    dashed_params = params.filter {|p| p.param_type != :indexed }
    dashed_params_string = dashed_params.size >= 0 ? "[options]" : ""
    
    puts ["Usage: ", File.basename($0), indexed_param_string, dashed_params_string].join(" ").gsub(/(\s+)/,' ')
    column_sizes = [
      params.map{|p|p.name.size}.concat(["Arguments:".size]).max, 
      params.map{|p|p.short_name.size}.concat(["Alias".size]).max 
    ]
    if indexed_params.size > 0
      puts
      puts "#{"Arguments:".ljust(column_sizes[0]+4)}   #{"Alias".ljust(column_sizes[1])}  Description"
      for param in indexed_params
        puts "  #{param.name.ljust(column_sizes[0]+2)}   -#{param.short_name.ljust(column_sizes[1])}   #{param.description}"
      end
    end

    if dashed_params.size > 0
      puts
      puts "#{"Options:".ljust(column_sizes[0]+4)}   #{"Alias".ljust(column_sizes[1])}  Description"
      for param in dashed_params
        puts "  --#{param.name.ljust(column_sizes[0])}   -#{param.short_name.ljust(column_sizes[1])}   #{param.description}"
      end
    end
  end
end
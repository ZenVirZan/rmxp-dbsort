# RMXP DB Sort tool

![Ordered database example, animations tab](https://i.imgur.com/PxA7foA.png)
## Intro
RMXP DB Sort tool is a Ruby script that sorts the database, switch and variable entries by name. It also examines all database entries, maps, events and common events and updates the references so that no functionality is changed.

## Prerequisites
A local Ruby installation to execute the script.

## Usage
1. Back up your project
1. If you're sorting the project you're currently working on, **make sure to close the editor first** 
1. Run `ruby rmxp-db-sort.rb <project directory> -o <output directory>`.

Use the `--help` to display all script parameters.

_Note: As the IDs will be changing order, batch variable and switch assignment event commands may end up splitting into smaller groups, or individually, depending on how the sorted layout groups them together. By default, the best effort is made to keep them in bulk calls where possible, but this can be toggled to force any bulk commands that would be split into smaller bulk commands to instead be split individually._